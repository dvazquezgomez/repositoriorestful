var express = require('express');

var port = process.env.PORT || 3000;
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);
console.log("API molona escuchando en el puerto " + port);

app.get("/apitechu/v1",
  function(req, res) {
    console.log("GET /apitechu/v1");
    res.send({"msg" : "Hola desde APITechU"});
  }
);

app.get("/apitechu/v1/users",
  function(req, res) {
      console.log("GET /apitechu/v1/users");

      //res.sendFile('./usuarios.json'); //DEPRECATED
      res.sendFile('usuarios.json', {root: __dirname});

      // var users = require('./usuarios.json');
      // res.send(users);
  }
);

app.post("/apitechu/v1/users",
    function (req, res) {
      console.log("POST /apitechu/v1/users");
      console.log("first_name is " + req.body.first_name);
      console.log("last_name is " + req.body.last_name);
      console.log("country is " + req.body.country);

      var newUser = {
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "country" : req.body.country
      };

      var users = require('./usuarios.json');
      // Array
      users.push(newUser);;
      writeUserDataToFile(users);
      console.log("Usuario guardado con éxito");
      res.send({"msg" : "Usuario guardado con éxito"});
    }
);

app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params.id);

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);
    writeUserDataToFile(users);
    console.log("Usuario borrado");
    res.send({"msg" : "Usuario borrado"});
  }
);

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
      function(err) {
        if(err) {
          console.log(err);
        } else {
          console.log("Datos escritos en el archivo");
        }
      }
  );
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query string");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);
  }
);

app.post("/apitechu/v1/login",
    function (req, res) {
      var message;

      console.log("POST /apitechu/v1/login");

      console.log("email is " + req.body.email);
      console.log("password is " + req.body.password);

      var requestedUser = {
        "email" : req.body.email,
        "password" : req.body.password
      };

      // Cargamos los usuarios.
      var users = require('./usuarios.json');
      for (user of users) {
        if (requestedUser.email == user.email) {
          if (requestedUser.password == user.password) {
            user.logged = true;
            writeUserDataToFile(users);
            message = '{"mensaje" : "Login correcto", "idUsuario" : ' + user.id + '}';
            break;
          } else {
            message = '{"mensaje" : "Login incorrecto"}';
            break;
          }
        } else {
          message = '{"mensaje" : "Login incorrecto"}';
        }
      }

      res.send(message);
    }
);

app.post("/apitechu/v1/logout",
    function (req, res) {
      var message;

      console.log("POST /apitechu/v1/logout");

      console.log("id is " + req.body.id);

      // Cargamos los usuarios.
      var users = require('./usuarios.json');
      for (user of users) {
        if (req.body.id == user.id) {
          delete user.logged;
          writeUserDataToFile(users);
          message = '{"mensaje" : "Logout correcto", "idUsuario" : ' + user.id + '}';
          break;
        } else {
          message = '{"mensaje" : "Logout incorrecto"}';
        }
      }

      res.send(message);
    }
);
